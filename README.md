# non_ascii

This package aims to help developers who manipulate strings. It provides some functions to manipulate strings using *Unicode graphemes*. With the Rust std library,you can manipulate strings using chars and bytes but not directly with graphemes. So here is `non_ascii`. Basically, it's just a wrapper around `unicode-segmentation`.

# Examples

```rust
use non_ascii::NonAscii;
let mut string = String::from("élégant");

// The following should panic because 'é' takes 2 bytes
// string.insert(1, 'c');

string.safe_insert(1, 'c');
assert_eq!(string.as_str(), "éclégant");
```

```rust
use non_ascii::NonAscii;
let mut string = String::from("élégant");

// The following should panic
// string.remove(1);

assert_eq!(string.safe_remove(1), 'l');
```

```rust
use non_ascii::NonAscii;
let mut string = String::from("élégant");

assert_eq!(string.graph_len(), 7);
assert_eq!(string.len(), 11);
```
```rust
use non_ascii::NonAscii;
let mut string = String::from("élégant");

assert!(string.graph_len() != string.len());
```